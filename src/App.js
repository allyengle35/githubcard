import React, { Component } from "react";
import "./App.css";
import { Button, Card, Image } from "semantic-ui-react";

const GIT_USER = accountURL => `https://api.github.com/users/${accountURL}`;

class App extends Component {
  state = {
    user: {},
    active: false
  };

  handleToggle = () => {
    if (this.state.active === true) {
      this.setState({ active: false });
    } else {
      this.setState({ active: true });
    }
    fetch(GIT_USER("Ally35"))
      .then(response => response.json())
      .then(data => {
        this.setState({ user: data});
      });
  };

  render() {
    return (
      <div className="App">
        <Button onClick={this.handleToggle}>Toggle</Button>
        {this.state.active === true && (
          <Card>
            <Image src={this.state.user.avatar_url} alt="User Avatar" />
            <Card.Content>
              <Card.Header>{this.state.user.login}</Card.Header>
              <Card.Description>{this.state.user.bio}</Card.Description>
            </Card.Content>
          </Card>
        )}
      </div>
    );
  }
}

export default App;
